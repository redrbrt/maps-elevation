$(function () {

  $('.open-file button').on('click', function () {
    $('[type="file"]')[0].click();
  });

  $('[type="file"]').on('change', function () {
    MapController.validateFile(this.files[0]);
  });

});

var MapControllerClass = class {

  constructor() {
    this.sizes = { small: 20, medium: 26, large: 32, active: 'medium' };
    this.settings = {
      strokeRoute: {
        strokeColor: '#0f0', // green
        strokeOpacity: .9,
        strokeWeight: 3
      },
      baseMarkers: {
        pointer: { size: this.sizes.active, filename: 'pin-user.png' },
        start: { size: this.sizes.active, filename: 'circle-pin3.png' },
        end: { size: this.sizes.active, filename: 'circle-pin-race.png' }
      }
    };
  }

  validateFile(file) {
    let name, size;
    if (typeof file == 'object' && file.constructor.name == 'File' && file.name.toLowerCase().endsWith('.gpx')) {
      $('body').addClass('loading');
      name = file.name;
      size = Math.round(file.size / 1024) + ' KB';
      this.loadMap(file);
    } else {
      console.warn('Loading invalid file: %o', file);
    }
    $('.open-file .file-name').text(name || '');
    $('.open-file .file-size').text(size || '');
  }

  loadMap(file) {
    this.$map = $('.map').gpxMap();
    this.$map.loadFile(file, (route) => {
      $('body').removeClass('loading');
      this.route = route;
      this.drawBaseIcons(route);
      ElevationController.drawGraphicElevation(route);
    }, this.settings.strokeRoute); // optional stroke properties
  }

  drawBaseIcons(route) { // start|end points and circle dot altimetry
    this.settings.baseMarkers.pointer.position = route[20];
    this.settings.baseMarkers.start.position = route[0];
    this.settings.baseMarkers.end.position = route[route.length - 1];
    $.each(this.settings.baseMarkers, (key, icon) => {
      var marker = new google.maps.Marker({
        position: icon.position,
        map: this.$map.getMapInstance(),
        zIndex: 10,
        icon: {
          url: 'icons/maps-' + this.sizes[icon.size] + '/' + icon.filename,
          size: new google.maps.Size(this.sizes[icon.size], this.sizes[icon.size]), // width and height
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(this.sizes[icon.size] / 2, this.sizes[icon.size]) // icon point on map
        }
      });
      key == 'pointer' ? (this.pointerMarker = marker) : marker.addListener('click', () => {
        (new google.maps.InfoWindow({
          content: "<span>" + key + "</span>"
        })).open(this.$map.getMapInstance(), marker);
      });
    }); // each icons
  }

  onHoverAltimetryGraphic(routeIndex) {
    if (this.pointerMarker && this.route) {
      if (routeIndex >= 0) {
        this.pointerMarker.setVisible(true);
        this.pointerMarker.setPosition(this.route[routeIndex]);
      } else {
        this.pointerMarker.setVisible(false);
      }
    }
  }

}, MapController = new MapControllerClass();
