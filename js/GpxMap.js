$.fn.gpxMap = function (options) {
  var _this = this, maps = [], mapSettings = $.extend({
//    center: { lat: 21.126066, lng: -99.638199 },
    mapTypeId: 'hybrid',
    zoom: 12
  }, options);

  this.each(function () {
    maps.push(new google.maps.Map($(this).get(0), mapSettings));
  });

  this.getMapInstance = function (index) {
    return index ? maps[index] : maps[0];
  };

  this.loadFile = function (file, callbackAfterLoad, polylineSettings) {
    var polylineSettings = $.extend({
      maxNodes: 1000
    }, polylineSettings || { });
    if (typeof file != 'object' || !('name' in file) || file.name.split('.').pop() != 'gpx') {
      console.warn('selected file is not valid gpx format');
      return false;
    }
    var reader = new FileReader();
    reader.readAsText(file);
    reader.onloadend = function () {
      var $xml = $(reader.result);
      var $points = $xml.find('trkpt'), n = 1; // this instrucction depends .gpx filesize
      var insertEvery = $points.length > polylineSettings.maxNodes ? Math.ceil($points.length / polylineSettings.maxNodes) : null;
      var route = $points.map(function (idx) {
        if (!insertEvery || insertEvery == n++) {
          n = 1;
          var node = { lat: parseFloat($(this).attr('lat')), lng: parseFloat($(this).attr('lon')), ele: parseFloat($(this).find('ele').text()) || null };
          return node.lat && node.lng ? node : null;
        }
        return null;
      }).get();
      attachDistance(route);
      drawRoute(route, polylineSettings);
      var resultAfter = callbackAfterLoad ? callbackAfterLoad.call(_this, route) : undefined;
      if (!resultAfter && resultAfter !== undefined) {
        return false; // cancel by user
      }
    };
  };

  var attachDistance = function (route) {
    var totalMts = 0, prevPoint;
    $.each(route, function (k, point) {
      if (k) {
        totalMts += google.maps.geometry.spherical.computeLength((new google.maps.Polyline({ path: [prevPoint, point] })).getPath());
      }
      this['km'] = Math.round(totalMts) / 1000;
      this['mi'] = Math.round(this.km * 0.621371 * 1000) / 1000;
      prevPoint = point;
    });
  };

  /* --------------- Load and draw GPX tracks ------------------ */

  var drawRoute = function (route, polylineSettings) {
    var polylineSettings = $.extend({
      strokeColor: 'rgb(90, 220, 30)',
      strokeOpacity: .8,
      strokeWeight: 3,
      geodesic: true,
      path: route,
    }, polylineSettings);
    $.each(maps, function () {
      var polyline = new google.maps.Polyline(polylineSettings);
      polyline.setMap(this);
      var x = { start: route[0], end: route[route.length - 1] };
      this.setCenter({ lat: (x.start.lat + x.end.lat) / 2, lng: (x.start.lng + x.end.lng) / 2 }); // centering view between start and finish points
    });
  };

  return this;
};