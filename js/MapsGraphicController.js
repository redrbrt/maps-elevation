var ElevationControllerClass = class {

  constructor() {
    this.$canvas = $('.altimetry-chart canvas');
    this.settings = {
      maxPointsShow: 300, // numeric, (set to 0 to show all points)
      currentPoint: 'rgba(90, 150, 250, 1)',
      lineColor: 'rgba(50, 150, 50, 1)',
      areaColor: this.getGradientBackground('rgba(240, 210, 60, 0.6)', 'rgba(0, 127, 0, 0.6)')
    };
  }

  getGradientBackground(topColor, bottomColor) {
    var gradientAreaColor = this.$canvas.get(0).getContext("2d").createLinearGradient(150, 0, 150, 300);
    gradientAreaColor.addColorStop(.15, topColor); // top
    gradientAreaColor.addColorStop(.6, bottomColor); // bottom
    return gradientAreaColor;
  }

  drawGraphicElevation(route) {
    var data = {
      points: [],
      elevation: [],
      distance: [],
      accumulated: 0,
      pointMaxElevation: { ele: 0 }
    };
    var insertEvery = this.settings.maxPointsShow && route.length > this.settings.maxPointsShow ? Math.ceil(route.length / this.settings.maxPointsShow) : null, n = 0, prevPoint;
    data.points = $.map(route, function (point, i) {
      if (!i || !insertEvery || insertEvery == n++) {
        point.original_index = i;
        if (point.ele > data.pointMaxElevation.ele) {
          data.pointMaxElevation = point;
        }
        if (prevPoint) {
          point.accumulated = (point.ele > prevPoint.ele) ? Math.round((point.ele - prevPoint.ele) * 10) / 10 : 0;
          data.accumulated += point.accumulated;
        }
        data.elevation.push(point.ele);
        data.distance.push(Math.round(point.km * 10) / 10);
        n = 1;
        prevPoint = point;
        return point;
      }
      return null;
    });
    var chartConfig = {
      type: 'line',
      data: {
        labels: data.distance,
        datasets: [{
            type: 'line',
            label: 'Altimetria',
            data: data.elevation,
            borderWidth: 3,
            backgroundColor: this.settings.areaColor,
            borderColor: this.settings.lineColor,
            pointRadius: 0,
            pointBackgroundColor: this.settings.currentPoint, // datasets bar will show this point styles
            pointBorderColor: this.settings.currentPoint
          }]
      },
      options: {
        maintainAspectRatio: false,
        customLine: {
          color: 'black'
        },
        legend: {
          labels: {
            fontSize: 18,
//            boxWidth: 0
          }
        },
        hover: {
          intersect: false,
          mode: 'nearest'
        },
        tooltips: {
          intersect: false,
          mode: 'index',
          yAlign: 'bottom',
          position: 'custom',
          displayColors: false,
          callbacks: {
            title: function (tooltipItem, data) {
              return Math.round(tooltipItem[0].yLabel) + ' mts';
            },
            label: function (tooltipItem, data) {
              return tooltipItem.xLabel;
            }
          }
        },
        scales: {
          xAxes: [{
              barPercentage: 1,
              categoryPercentage: 1,
              gridLines: {
                color: 'rgba(100,100,100, .25)',
                borderDash: [5, 5]
              },
              ticks: {
                padding: 10,
                maxTicksLimit: 10,
                maxRotation: 0,
                callback: function (value, index, values) {
                  return value + ' km';
                }
              }
            }],
          yAxes: [{
              gridLines: {
                color: 'rgba(100,100,100, .3)',
                borderDash: [15, 5]
              },
              ticks: {
                fontStyle: 'bold',
                fontSize: 14,
                maxTicksLimit: 4,
                suggestedMax: data.pointMaxElevation.ele + 10
              }
            }]
        },
        onHover: function (e, activePoints, b) {
          var compressedIndex = activePoints[0] && activePoints[0]._index, realIndex = null;
          if (compressedIndex) {
            realIndex = data.points[compressedIndex].original_index;
          }
          MapController.onHoverAltimetryGraphic(realIndex);
        }
      }
    };
    this.registerCustomTooltipPosition();
    var AltimetryChart = new Chart(this.$canvas, chartConfig);

    var resizeEvent = 'resize.altimetry';
    $(window).off(resizeEvent).on(resizeEvent, function () {
      AltimetryChart.resize();
    }).trigger(resizeEvent);
  }

  registerCustomTooltipPosition() {
    Chart.Tooltip.positioners.custom = function (e, position) {
      if (!e.length) {
        return false;
      }
      return { x: position.x, y: e[0]._model.y - 10 };
      return { x: position.x, y: position.y }; // follow cursor
    };
  }

}, ElevationController = new ElevationControllerClass();